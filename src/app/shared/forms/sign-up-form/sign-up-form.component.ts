import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sign-up-form',
  templateUrl: './sign-up-form.component.html',
  styleUrls: ['./sign-up-form.component.css']
})
export class SignUpFormComponent implements OnInit {

  statusMessage: string;
  signUpForm = this.formBuilder.group({
    email: ['', Validators.required],
    password: ['', [Validators.required, Validators.minLength(8)]],
    repeatedPassword: ['', Validators.required]
  });

  constructor(private router: Router, private formBuilder: FormBuilder) { }

  signUp() {
    console.log('signIn() works');
    console.log('email: ' + this.signUpForm.value.email + ', password: ' + this.signUpForm.value.password );

    let isSignInSuccessful = true;
    isSignInSuccessful = true;

    if (isSignInSuccessful) {
      this.statusMessage = 'Loggin in..';
      this.router.navigateByUrl('/sign-in');
    } else {
      this.statusMessage = 'Wrong login or password';
    }
  }

  ngOnInit() {
  }

}
