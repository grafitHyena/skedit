import { Component} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in-form',
  templateUrl: './sign-in-form.component.html',
  styleUrls: ['./sign-in-form.component.css']
})
export class SignInFormComponent {

  statusMessage: string;
  signInForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
  });

  constructor(private router: Router) { }

  signIn() {
    console.log('signIn() works');
    console.log('email: ' + this.signInForm.value.email + ', password: ' + this.signInForm.value.password );

    let isSignInSuccessful = true;

    if (isSignInSuccessful) {
      this.statusMessage = 'Loggin in..';
      this.router.navigateByUrl('/dashboard');
    } else {
      this.statusMessage = 'Wrong login or password';
    }
  }
}
